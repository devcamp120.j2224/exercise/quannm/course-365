"use strict";
$(document).ready(function() {
    // Vùng 1: Khai báo biến toàn cục
    var gCoursesDB = {
        description: "This DB includes all courses in system",
        courses: [
            {
                id: 1,
                courseCode: "FE_WEB_ANGULAR_101",
                courseName: "How to easily create a website with Angular",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-angular.jpg",
                teacherName: "Morris Mccoy",
                teacherPhoto: "images/teacher/morris_mccoy.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 2,
                courseCode: "BE_WEB_PYTHON_301",
                courseName: "The Python Course: build web application",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-python.jpg",
                teacherName: "Claire Robertson",
                teacherPhoto: "images/teacher/claire_robertson.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 5,
                courseCode: "FE_WEB_GRAPHQL_104",
                courseName: "GraphQL: introduction to graphQL for beginners",
                price: 850,
                discountPrice: 650,
                duration: "2h 15m",
                level: "Intermediate",
                coverImage: "images/courses/course-graphql.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: false
            },
            {
                id: 6,
                courseCode: "FE_WEB_JS_210",
                courseName: "Getting Started with JavaScript",
                price: 550,
                discountPrice: 300,
                duration: "3h 34m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 8,
                courseCode: "FE_WEB_CSS_111",
                courseName: "CSS: ultimate CSS course from beginner to advanced",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-css.jpg",
                teacherName: "Juanita Bell",
                teacherPhoto: "images/teacher/juanita_bell.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 14,
                courseCode: "FE_WEB_WORDPRESS_111",
                courseName: "Complete Wordpress themes & plugins",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-wordpress.jpg",
                teacherName: "Clevaio Simon",
                teacherPhoto: "images/teacher/clevaio_simon.jpg",
                isPopular: true,
                isTrending: false
            }
        ]
    }

    // Vùng 2: Vùng gán
    onPageLoading();

    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
        "use strict";
        console.log("Load trang!");
        loadCoursesToMostPopular();
        loadCoursesToTrending();
    }

    // Vùng 4: Khai báo hàm dùng chung
    // Hàm load dữ liệu khóa học popular
    function loadCoursesToMostPopular() {
        "use strict";
        var vCard = "";
        $('#div-most-popular').empty();
        for (var bI = 0; bI < gCoursesDB.courses.length; bI++) {
            if (gCoursesDB.courses[bI].isPopular == true) {
                vCard = 
                `<div class="col-sm-3">
                    <div class="card">
                        <div class="card-header p-0">
                            <img src="${gCoursesDB.courses[bI].coverImage}" class="card-img-top mx-auto d-block" alt="Course CSS"/>
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">
                                <a href="#">${gCoursesDB.courses[bI].courseName}</a> 
                            </h6>
                            <span>
                                <i class="far fa-clock"></i>
                            </span>
                            <span>${gCoursesDB.courses[bI].duration}</span>
                            <span>${gCoursesDB.courses[bI].level}</span>
                            <p class="card-text">
                                <span><b>$${gCoursesDB.courses[bI].price}</b></span>
                                <span style="text-decoration: line-through">$${gCoursesDB.courses[bI].discountPrice}</span>
                            </p>
                        </div>
                        <div class="card-footer">
                            <img class="rounded-circle img-thumbnail" src="${gCoursesDB.courses[bI].teacherPhoto}" alt="${gCoursesDB.courses[bI].teacherName}" width="50" height="50"/>
                            <span class="pl-3">${gCoursesDB.courses[bI].teacherName}</span>
                            <span><i class="far fa-bookmark float-right"></i></span>
                        </div>
                    </div>
                </div>`;
                $('#div-most-popular').append(vCard);
            }
        }
    }

    // Hàm load dữ liệu khóa học trending
    function loadCoursesToMostPopular() {
        "use strict";
        var vCard = "";
        $('#div-trending').empty();
        for (var bI = 0; bI < gCoursesDB.courses.length; bI++) {
            if (gCoursesDB.courses[bI].isTrending == true) {
                vCard = 
                `<div class="col-sm-3">
                    <div class="card">
                        <div class="card-header p-0">
                            <img src="${gCoursesDB.courses[bI].coverImage}" class="card-img-top mx-auto d-block" alt="Course CSS"/>
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">
                                <a href="#">${gCoursesDB.courses[bI].courseName}</a> 
                            </h6>
                            <span>
                                <i class="far fa-clock"></i>
                            </span>
                            <span>${gCoursesDB.courses[bI].duration}</span>
                            <span>${gCoursesDB.courses[bI].level}</span>
                            <p class="card-text">
                                <span><b>$${gCoursesDB.courses[bI].price}</b></span>
                                <span style="text-decoration: line-through">$${gCoursesDB.courses[bI].discountPrice}</span>
                            </p>
                        </div>
                        <div class="card-footer">
                            <img class="rounded-circle img-thumbnail" src="${gCoursesDB.courses[bI].teacherPhoto}" alt="${gCoursesDB.courses[bI].teacherName}" width="50" height="50"/>
                            <span class="pl-3">${gCoursesDB.courses[bI].teacherName}</span>
                            <span><i class="far fa-bookmark float-right"></i></span>
                        </div>
                    </div>
                </div>`;
                $('#div-trending').append(vCard);
            }
        }
    }


});
