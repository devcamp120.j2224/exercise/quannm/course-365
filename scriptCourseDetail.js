"use strict";
$(document).ready(function() {
    // Vùng 1: Khai báo biến toàn cục
    var gCoursesDB = {
        description: "This DB includes all courses in system",
        courses: [
            {
                id: 1,
                courseCode: "FE_WEB_ANGULAR_101",
                courseName: "How to easily create a website with Angular",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-angular.jpg",
                teacherName: "Morris Mccoy",
                teacherPhoto: "images/teacher/morris_mccoy.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 2,
                courseCode: "BE_WEB_PYTHON_301",
                courseName: "The Python Course: build web application",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-python.jpg",
                teacherName: "Claire Robertson",
                teacherPhoto: "images/teacher/claire_robertson.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 5,
                courseCode: "FE_WEB_GRAPHQL_104",
                courseName: "GraphQL: introduction to graphQL for beginners",
                price: 850,
                discountPrice: 650,
                duration: "2h 15m",
                level: "Intermediate",
                coverImage: "images/courses/course-graphql.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: false
            },
            {
                id: 6,
                courseCode: "FE_WEB_JS_210",
                courseName: "Getting Started with JavaScript",
                price: 550,
                discountPrice: 300,
                duration: "3h 34m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 8,
                courseCode: "FE_WEB_CSS_111",
                courseName: "CSS: ultimate CSS course from beginner to advanced",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-css.jpg",
                teacherName: "Juanita Bell",
                teacherPhoto: "images/teacher/juanita_bell.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 14,
                courseCode: "FE_WEB_WORDPRESS_111",
                courseName: "Complete Wordpress themes & plugins",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-wordpress.jpg",
                teacherName: "Clevaio Simon",
                teacherPhoto: "images/teacher/clevaio_simon.jpg",
                isPopular: true,
                isTrending: false
            }
        ]
    };

    var gNameCols = ["stt", "courseCode", "courseName", "price", "discountPrice", "duration", "level", "teacherName", "isPopular", "isTrending", "action"];

    const gSTT_COL = 0;
    const gCOURSE_CODE_COL = 1;
    const gCOURSE_NAME_COL = 2;
    const gPRICE_COL = 3;
    const gDISCOUNT_PRICE_COL = 4;
    const gDURATION_COL = 5;
    const gLEVEL_COL = 6;
    const gTEACHER_NAME_COL = 7;
    const gIS_POPULAR_COL = 8;
    const gIS_TRENDING_COL = 9;
    const gACTION_COL = 10;
    var gStt = 0;

    var gDataCourse = {};

    var gCoursesNew = [];

    // Vùng 2: Vùng gán
    onPageLoading();

    $('#btn-add-course').on('click', onBtnAddCourseClick);
    $('#btn-modal-add-course').on('click', onBtnModalAddCourseClick);
    $('#btn-modal-cancel').on('click', onBtnModalAddCancelClick);

    $(document).on('click', '#btn-edit-course', function() {
        onBtnEditCourseClick(this);
    });
   $('#btn-modal-edit-course').on('click', onBtnModalEditCourseClick);

   $(document).on('click', '#btn-delete-course', function() {
    onBtnDeleteCourseClick(this);
    });
    $('#btn-modal-delete-course').on('click', onBtnModalDeleteCourseClick);

    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
        "use strict";
        console.log("Load trang!");
        loadDataCoursesToTable();
    }

    // Hàm xử lý khi ấn nút Add course
    function onBtnAddCourseClick() {
        "use strict";
        console.log("Add Course");
        $('#modal-add-course').modal('show');
    }

    // Hàm xử lý khi Bấm Add course trong modal
    function onBtnModalAddCourseClick() {
        "use strict";
        // B0: Tạo đối tượng lưu trữ dữ liệu
        var vNewCourseObj = {
            id: 0,
            courseCode: "",
            courseName: "",
            price: 0,
            discountPrice: 0,
            duration: "",
            level: "",
            coverImage: "",
            teacherName: "",
            teacherPhoto: "",
            isPopular: "",
            isTrending: ""
        };
        // B1: Thu thập dữ liệu từ input in modal
        getDataFromInputInModalAdd(vNewCourseObj);
        //console.log(vNewCourseObj);
        // B2: Validate dữ liệu
        var vIsCheck = validateData(vNewCourseObj);
        console.log(vIsCheck);
        if (vIsCheck == true) {
            // B3: Add mới vào biến toàn cục
            gCoursesDB.courses.push(vNewCourseObj);
            console.log(gCoursesDB.courses);
            // B4: Xử lý hiển thị
            loadNewDataCoursesToTable(gCoursesDB.courses);
            resertMoDalAddCourse();
            $('#modal-add-course').modal('hide');
        }      
    }

    // Hàm xử lý khi ấn nút cancel trong modal Add
    function onBtnModalAddCancelClick() {
        "use strict";
        resertMoDalAddCourse();
    }

    // Hàm xử lý khi ấn nút Edit course
    function onBtnEditCourseClick(paramEle) {
        "use strict";
        console.log("Edit Course");
        $('#modal-edit-course').modal('show');
        var vRowClick = $(paramEle).closest('tr');
        var vTable = $('#table-courses').DataTable();
        var vData = vTable.row(vRowClick).data();
        console.log(vData);
        gDataCourse = vData;
        showDataInEditModal(vData);
    }

    // Hàm xử lý khi Bấm Edit course trong modal
    function onBtnModalEditCourseClick() {
        "use strict";
        // B0: Tạo đối tượng lưu trữ dữ liệu
        //console.log(gDataCourse);
        var vEditCourseObj = {
            id: gDataCourse.id,
            courseCode: "",
            courseName: "",
            price: 0,
            discountPrice: 0,
            duration: "",
            level: "",
            coverImage: "",
            teacherName: "",
            teacherPhoto: "",
            isPopular: "",
            isTrending: ""
        };
        // B1: Thu thập dữ liệu từ input in modal
        getDataFromInputInModalEdit(vEditCourseObj);
        //console.log(vEditCourseObj);
        // B2: Validate dữ liệu
        var vIsCheck = validateData(vEditCourseObj);
        console.log(vIsCheck);
        if (vIsCheck == true) {
            // B3: Chỉnh sửa trên biến toàn cục
            var bI = 0;
            while (bI < gCoursesDB.courses.length) {
                if (vEditCourseObj.id == gCoursesDB.courses[bI].id) {
                    gCoursesDB.courses[bI] = vEditCourseObj;
                }
                bI++;
            }
            console.log(gCoursesDB.courses);
            // B4: Xử lý hiển thị
            loadNewDataCoursesToTable(gCoursesDB.courses);
            //resertMoDalAddCourse();
            $('#modal-edit-course').modal('hide');
            gDataCourse = {};
        }
    }

    // Hàm xử lý khi ấn nút Delete course
    function onBtnDeleteCourseClick(paramEle) {
        "use strict";
        console.log("Delete Course");
        $('#modal-confirm-delete').modal('show');
        var vRowClick = $(paramEle).closest('tr');
        var vTable = $('#table-courses').DataTable();
        var vData = vTable.row(vRowClick).data();
        console.log(vData);
        gDataCourse = vData;
    }

    // Hàm xử lý khi ấn confirm trong modal delete
    function onBtnModalDeleteCourseClick() {
        "use strict";
        var vCount = 0;
        while (vCount < gCoursesDB.courses.length) {
            if (gDataCourse.id != gCoursesDB.courses[vCount].id) {
                gCoursesNew.push(gCoursesDB.courses[vCount]);
            }
            vCount++;
        }
        console.log(gCoursesNew);
        loadNewDataCoursesToTable(gCoursesNew);
        $('#modal-confirm-delete').modal('hide');
        gCoursesNew = [];
    }

    // Vùng 4: Khai báo hàm dùng chung
    // Hàm đổ data vào Table
    function loadDataCoursesToTable() {
        "use strict";
        $('#table-courses').DataTable({
            data: gCoursesDB.courses,
            columns: [
                { data: gNameCols[gSTT_COL] },
                { data: gNameCols[gCOURSE_CODE_COL] },
                { data: gNameCols[gCOURSE_NAME_COL] },
                { data: gNameCols[gPRICE_COL] },
                { data: gNameCols[gDISCOUNT_PRICE_COL] },
                { data: gNameCols[gDURATION_COL] },
                { data: gNameCols[gLEVEL_COL] },
                { data: gNameCols[gTEACHER_NAME_COL] },
                { data: gNameCols[gIS_POPULAR_COL] },
                { data: gNameCols[gIS_TRENDING_COL] },
                { data: gNameCols[gACTION_COL] }
            ],
            columnDefs: [
                {
                    targets: gSTT_COL,
                    render: function() {
                        gStt++;
                        return gStt;
                    }
                },
                {
                    targets: gACTION_COL,
                    defaultContent: '<i class="fas fa-edit" style="color: green" alt="Edit Course" id="btn-edit-course"></i> &nbsp <i class="fas fa-trash" style="color: red" alt="Delete Course" id="btn-delete-course"></i>'
                }
            ]
        });
    }

    // Hàm thu thập dữ liệu từ input trên modal add
    function getDataFromInputInModalAdd(paramObj) {
        "use strict";
        paramObj.id = getNextId();
        paramObj.courseCode = $.trim($('#inp-add-course-code').val());
        paramObj.courseName = $.trim($('#inp-add-course-name').val());
        paramObj.price = $.trim($('#inp-add-price').val());
        paramObj.discountPrice = $.trim($('#inp-add-discount-price').val());
        paramObj.duration = $.trim($('#inp-add-duration').val());
        paramObj.level = $.trim($('#inp-add-level').val());
        paramObj.coverImage = $.trim($('#inp-add-cover-image').val());
        paramObj.teacherName = $.trim($('#inp-add-teacher-name').val());
        paramObj.teacherPhoto = $.trim($('#inp-add-teacher-photo').val());
        paramObj.isPopular = $("input[name=popular]:checked").val();
        paramObj.isTrending = $("input[name=trending]:checked").val();
    }

    // Hàm thu thập dữ liệu từ input trên modal add
    function getDataFromInputInModalEdit(paramObj) {
        "use strict";
        paramObj.courseCode = $.trim($('#inp-edit-course-code').val());
        paramObj.courseName = $.trim($('#inp-edit-course-name').val());
        paramObj.price = $.trim($('#inp-edit-price').val());
        paramObj.discountPrice = $.trim($('#inp-edit-discount-price').val());
        paramObj.duration = $.trim($('#inp-edit-duration').val());
        paramObj.level = $.trim($('#inp-edit-level').val());
        paramObj.coverImage = $.trim($('#inp-edit-cover-image').val());
        paramObj.teacherName = $.trim($('#inp-edit-teacher-name').val());
        paramObj.teacherPhoto = $.trim($('#inp-edit-teacher-photo').val());
        paramObj.isPopular = $("input[name=popular]:checked").val();
        paramObj.isTrending = $("input[name=trending]:checked").val();
    }

    // Hàm validate dữ liệu thu thập được
    function validateData(paramObj) {
        "use strict";
        if (paramObj.courseCode == "") {
            alert("Chưa nhập Course Code");
            return false;
        }
        if (checkTrungLapCourseCode(paramObj.courseCode) == false) {
            alert("Course Code đã tồn tại");
            return false;
        }
        if (paramObj.courseName == "") {
            alert("Chưa nhập Course Name");
            return false;
        }
        if (paramObj.price == "") {
            alert("Chưa nhập Price");
            return false;
        }
        var vChuyenSo = Number(paramObj.price);
        if (laSoNguyenLonHon0(vChuyenSo) == false) {
            alert("Price phải là số nguyên lớn hơn 0");
            return false;
        }
        if (paramObj.duration == "") {
            alert("Chưa nhập Duration");
            return false;
        }
        if (paramObj.level == "") {
            alert("Chưa nhập Level");
            return false;
        }
        if (paramObj.coverImage == "") {
            alert("Chưa nhập Cover Image");
            return false;
        }
        if (paramObj.teacherName == "") {
            alert("Chưa nhập Teacher Name");
            return false;
        }
        if (paramObj.teacherPhoto == "") {
            alert("Chưa nhập Teacher Photo");
            return false;
        }
        if (paramObj.isPopular == null) {
            alert("Chưa chọn isPopular");
            return false;
        }
        if (paramObj.isTrending == null) {
            alert("Chưa chọn isTrending");
            return false;
        }
        return true;
    }

    // hàm sinh ra đc id tự tăng tiếp theo, dùng khi Thêm mới phần tử
    function getNextId() {
        var vNextId = 0;
        // Nếu mảng chưa có đối tượng nào thì Id = 1
        if(gCoursesDB.courses.length == 0) {
            vNextId = 1;
        }
        // Id tiếp theo bằng Id của phần tử cuối cùng + thêm 1    
        else {
            vNextId = gCoursesDB.courses[gCoursesDB.courses.length - 1].id + 1;
        }
        return vNextId;
    }

    // Hàm check trùng lặp course code
    function checkTrungLapCourseCode(paramCourseCode) {
        "use strict";
        for (var bI = 0; bI < gCoursesDB.courses.length; bI++) {
            if (paramCourseCode === gCoursesDB.courses[bI].courseCode) {
                console.log(paramCourseCode);
                console.log(gCoursesDB.courses[bI].courseCode);
                return false;
            }
            return true;
        }
    }

    // Hàm check là số nguyên
    function laSoNguyenLonHon0(paramNumber) {
        "use strict";
        if (!Number.isInteger(paramNumber) && paramNumber < 1) {
            return false;
        }
        return true;
    }

    // Hàm hiển thị lại datatable
    function loadNewDataCoursesToTable(paramObj) {
        "use strict";
        gStt = 0;
        var vTable = $('#table-courses').DataTable();
        vTable.clear();
        vTable.rows.add(paramObj);
        vTable.draw();
    }

    // Hàm xóa trắng modal Add course
    function resertMoDalAddCourse() {
        "use strict";
        $('#inp-add-course-code').val("");
        $('#inp-add-course-name').val("");
        $('#inp-add-price').val("");
        $('#inp-add-discount-price').val("");
        $('#inp-add-duration').val("");
        $('#inp-add-level').val("");
        $('#inp-add-cover-image').val("");
        $('#inp-add-teacher-name').val("");
        $('#inp-add-teacher-photo').val("");
        $("input[name=popular]").removeAttr('checked');
        $("input[name=trending]").removeAttr('checked');
    }

    // Hàm hiển thị data trên modal edit course
    function showDataInEditModal(paramData) {
        "use strict";
        $('#inp-edit-course-code').val(paramData.courseCode);
        $('#inp-edit-course-name').val(paramData.courseName);
        $('#inp-edit-price').val(paramData.price);
        $('#inp-edit-discount-price').val(paramData.discountPrice);
        $('#inp-edit-duration').val(paramData.duration);
        $('#inp-edit-level').val(paramData.level);
        $('#inp-edit-cover-image').val(paramData.coverImage);
        $('#inp-edit-teacher-name').val(paramData.teacherName);
        $('#inp-edit-teacher-photo').val(paramData.teacherPhoto);
        if (paramData.isPopular === true) {
            $("input[name=popular]").removeAttr('checked');
            $("#inp-edit-popular-true").attr("checked", true);
        }
        else {
            $("input[name=popular]").removeAttr('checked');
            $("#inp-edit-popular-false").attr("checked", true);
        }
        if (paramData.isTrending === true) {
            $("input[name=trending]").removeAttr('checked');
            $("#inp-edit-trending-true").attr("checked", true);
        }
        else {
            $("input[name=trending]").removeAttr('checked');
            $("#inp-edit-trending-false").attr("checked", true);
        }
    }

});